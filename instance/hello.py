from flask import Flask, render_template, request, jsonify
import atexit
import os
import json
import boto3

app = Flask(__name__, static_url_path='')

@app.route('/')
def root():
    return render_template("index.html")


@app.route('/create', methods=['POST','GET'])
def create():
    ec2 = boto3.resource('ec2')
    instance = ec2.create_instances(
        ImageId='ami-759bc50a',
        MinCount=1,
        MaxCount=1,
        InstanceType='t2.micro')
    print instance[0].id
    return render_template("index.html")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
